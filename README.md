## How to run
To run PCA, SVD, and t-SNE, we need to use Python 3, and have the following packages installed:
1. Numpy
2. Matplotlib
3. Scikit-Learn

To run PCA, execute the command
```python
python3 pca_main.py --file <file> --startcol <column where data starts> --endcol <column where data ends> --labelcol <column containing label data - must be AFTER data columns> --suffix <filename suffix with which to save the scatter plot>
```

Similarly, to run SVD, `python3 svd.py --file <file> --startcol <COL> --endcol <COL> --labelcol <COL> --suffix <SUFFIX>`, and to run t-SNE, `python3 tsne.py --file <file> --startcol <COL> --endcol <COL> --labelcol <COL> --suffix <SUFFIX>`.

An example invocation of PCA is as follows:
```python
python3 pca_main.py --file pca_a.txt --startcol 0 --endcol 3 --labelcol 4 --suffix a
```
