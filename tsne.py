import sys
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from sklearn.manifold import TSNE

def setup_argparse():
    parser = ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="File containing data to run PCA on", default="")
    parser.add_argument("-s", "--startcol", type=int, help="Column in file from where data begins (zero-indexed)", default=0)
    parser.add_argument("-e", "--endcol", type=int, help="Column in file where data ends (zero-indexed)", default=-1)
    parser.add_argument("-l", "--labelcol", type=int, help="Column in file where labels are present (zero-indexed); labels should be the last column if they are present", default=-1)
    parser.add_argument("-x", "--suffix", type=str, help="Suffix to use when saving graph to disk", default=None)
    return parser

def plot(X, labels, suffix=None):
    uniq_labels = sorted(set(labels))
    fig = plt.figure(figsize=(15, 10))
    ax = fig.add_subplot(1, 1, 1)
    for label in uniq_labels:
        indexes = [idx for idx, l in enumerate(labels) if l == label]
        ax.scatter(X[indexes, 0], X[indexes, 1], label=label)
    plt.title('Disease scatter plot - t-SNE')
    plt.legend()
    if suffix:
        plt.savefig("tsne_{0}.png".format(suffix))
    plt.show()

def main():
    args = setup_argparse().parse_args()
    if not args.file:
        print("Please pass in a file name using the -f/--file flag")
        sys.exit(1)
    filename = args.file
    startcol, endcol = args.startcol, args.endcol
    labelcol = args.labelcol
    x, labels = [], []
    with open(filename, "r") as f:
        lines = [line.strip() for line in f.readlines()]
        has_labels = True if labelcol != -1 else False
        if lines:
            endcol = min(len(lines[0]), endcol + 1) if endcol != -1 else len(lines[0])
        for line in lines:
            splits = line.strip().split("\t")
            if line:
                x.append([float(n) for n in splits[startcol:endcol]])
                if has_labels:
                    labels.append(str(splits[labelcol]))
    x = np.asarray(x)
    x_mean = np.mean(x.T, axis=1)
    x_centered = x - x_mean
    tsne = TSNE()
    # Fit centered x or not?
    x_projected = tsne.fit_transform(x)
    plot(x_projected, labels, suffix=args.suffix)
    return

if __name__ == "__main__":
    main()

