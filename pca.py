import numpy as np

class PCA(object):
    _x = None
    _eigen_values = None
    _eigen_vectors = None

    def __init__(self):
        pass

    def _cov(self, X, ridx, cidx):
        col1, col2 = X[ridx], X[cidx]
        rmean = self._array_mean(col1)
        cmean = self._array_mean(col2)
        rc = np.asarray([x - rmean for x in col1])
        cc = np.asarray([x - cmean for x in col2])
        numerator = sum( rc * cc )
        denominator = len(X[0]) - 1
        return numerator / denominator

    def _array_mean(self, x):
        return sum(x) / len(x)

    def covariance(self, X):
        rows, cols = len(X), len(X[0])
        cov = np.zeros((rows, rows))
        for ridx in range(rows):
            for cidx in range(rows):
                cov[ridx][cidx] = self._cov(X, ridx, cidx)
        return cov

    def transform(self, X, dims=2):
        X = np.asarray(X)
        self._x = X
        x_mean = np.mean(X.T, axis=1)
        x_centered = X - x_mean
        x_cov = self.covariance(x_centered.T)
        x_vals, x_vecs = np.linalg.eig(x_cov)
        self._eigen_values = x_vals
        self._eigen_vectors = x_vecs
        t_dims = min(dims, len(x_vals))
        x_project = np.dot(x_vecs.T, x_centered.T).T
        # Pick the n (= t_dims) largest values from the eigenvalues,
        # and only consider them for PCA.
        indices = sorted(np.argpartition(x_vals, -abs(t_dims))[-abs(t_dims):])
        return x_project[:, indices]

def main():
    m = [[4, 5, 7], [9, 8, 1], [4, 79, 81]]
    print(m)
    pca = PCA()
    m_pca = pca.transform(m)
    print("calculated pca", m_pca)
    return

if __name__ == "__main__":
    main()
